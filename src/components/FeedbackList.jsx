import React from 'react'
import { useContext } from 'react';
import FeedbackItem from './FeedbackItem';
import { motion, AnimatePresence } from 'framer-motion'
import FeedbackContext from '../context/FeedbackContext';
import Spinner from '../components/Spinner'

function FeedbackList() {
    const {feedback, isLoading} = useContext(FeedbackContext)
    
    if (!isLoading && (!feedback || feedback.length === 0)) {
        return <p>no feedback yet</p>
    }
    return (
        !isLoading ? (
        <div className="feedback-list">
            <AnimatePresence>
                {feedback.map((item) => (
                    <motion.div key={item.id} initial={{ opacity: 0 }} animate={{ opacity: 1 }} exit={{ opacity: 0 }}>
                        <FeedbackItem key={item.id} item={item} />
                    </motion.div>
                ))}
            </AnimatePresence>
        </div>)
        : (<Spinner/>)
    )
}

export default FeedbackList;